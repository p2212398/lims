from django.db import models
from recipes.models import Recipe

class NutritionAnalysis(models.Model):
    recipe = models.ForeignKey(Recipe, on_delete=models.CASCADE)
    nutrient_comparison = models.JSONField()  # Comparaison nutritionnelle
    health_score = models.FloatField()
    suggestions = models.TextField()  # Suggestions d'amélioration

    def __str__(self):
        return f"Analysis of {self.recipe.name}"
