from django.shortcuts import render
from .models import NutritionAnalysis

def analysis_detail(request, recipe_id):
    analysis = NutritionAnalysis.objects.get(recipe__id=recipe_id)
    return render(request, 'nutrition_analysis/analysis_detail.html', {'analysis': analysis})
