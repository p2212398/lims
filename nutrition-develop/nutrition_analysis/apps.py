from django.apps import AppConfig


class NutritionAnalysisConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nutrition_analysis'
