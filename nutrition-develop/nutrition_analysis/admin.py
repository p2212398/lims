from django.contrib import admin
from .models import NutritionAnalysis

admin.site.register(NutritionAnalysis)
