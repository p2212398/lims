from django import forms

class RecipeForm(forms.Form):
    ingredients = forms.CharField(
        widget=forms.Textarea(attrs={"rows": 6, "cols": 50,'class':'form-control'}),
        label="Liste des ingrédients (JSON)",
        help_text="Entrez la liste au format JSON (exemple : [{\"name\": \"sucre\", \"quantity\": 50, \"unit\": \"g\"}])",

    )