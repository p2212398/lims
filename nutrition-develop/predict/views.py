import json
from django.shortcuts import render
from .forms import RecipeForm

# Fonction pour obtenir les données nutritionnelles (mockée ici)
def get_nutritional_data(ingredient_name):
    database = {
        "sucre": {"energy_100g": 400, "proteins_100g": 0, "sugars_100g": 99, "fiber_100g": 0, "saturated-fat_100g": 0, "sodium_100g": 0},
        "farine": {"energy_100g": 364, "proteins_100g": 10, "sugars_100g": 0, "fiber_100g": 2.7, "saturated-fat_100g": 0, "sodium_100g": 2},
        "beurre": {"energy_100g": 717, "proteins_100g": 0.85, "sugars_100g": 0, "fiber_100g": 0, "saturated-fat_100g": 51, "sodium_100g": 11},
        "œufs": {"energy_100g": 143, "proteins_100g": 12.6, "sugars_100g": 0.37, "fiber_100g": 0, "saturated-fat_100g": 3.1, "sodium_100g": 142},
    }
    return database.get(ingredient_name.lower(), {})

# Traitement des ingrédients pour calculer les nutriments totaux
def process_ingredients(ingredients_json):
    try:
        ingredients = json.loads(ingredients_json)
    except json.JSONDecodeError:
        return {"error": "Format JSON invalide"}
    
    total_nutrients = {
        "energy": 0,
        "proteins": 0,
        "sugars": 0,
        "fiber": 0,
        "saturated_fat": 0,
        "sodium": 0,
    }

    for ingredient in ingredients:
        data = get_nutritional_data(ingredient["name"])
        multiplier = ingredient["quantity"] / 100  # Conversion pour 100 g
        for key in total_nutrients.keys():
            total_nutrients[key] += data.get(f"{key}_100g", 0) * multiplier

    return total_nutrients

# Vue principale pour prédire le NutriScore
def predict_nutriscore_view(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            ingredients_json = form.cleaned_data["ingredients"]
            nutrients = process_ingredients(ingredients_json)
            if "error" in nutrients:
                return render(request, "predict/predict_nutriscore.html", {"form": form, "error": nutrients["error"]})
            # Mock de prédiction du NutriScore (à remplacer par une vraie fonction)
            nutriscore = predict_nutriscore(nutrients)
            return render(request, "predict/nutriscore_result.html", {"nutriscore": nutriscore})
    else:
        form = RecipeForm()
    return render(request, "predict/predict_nutriscore.html", {"form": form})

# Fonction de prédiction NutriScore (mockée)
def predict_nutriscore(nutrients):
    # Exemple simple pour renvoyer un grade
    if nutrients["energy"] > 700:
        return "D"
    elif nutrients["sugars"] > 50:
        return "C"
    else:
        return "A"
