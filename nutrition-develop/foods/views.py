from django.shortcuts import render
from .models import Food
from django.shortcuts import render, get_object_or_404
from django.core.paginator import Paginator

def food_list(request):
    foods = Food.objects.all()
    paginator = Paginator(foods, 25)  # Show 25 contacts per page.
    page_number = request.GET.get("page")
    foods = paginator.get_page(page_number)
    return render(request, 'foods/food_list.html', {'foods': foods})
