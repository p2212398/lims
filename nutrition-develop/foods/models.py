from django.db import models

class Food(models.Model):
    name = models.CharField(max_length=300,blank=True, null=True)
    category = models.CharField(max_length=300,blank=True, null=True)
    food_group = models.CharField(max_length=300,blank=True, null=True)
    food_type = models.CharField(max_length=300,blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    name_scientific = models.CharField(max_length=300,blank=True, null=True)
    food_db_id=models.CharField(max_length=300,blank=True, null=True)
    def __str__(self):
        return self.name
