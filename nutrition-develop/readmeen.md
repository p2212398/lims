# nutrition analysis
##  Requirements
- Python
- venv
- Mysql

## How to run the project
 - install reqirements
 - at the root level of this project :
 - create venv:
 ` python3 -m venv env `
 - activate venv: `source env/bin/activate`
 - install dependencies: `pip install`
 - move to `nutrition/` directory
 - create `.env` file: `touch .env`
 - add content bellow to this file and update it to match your  server  configs:
 `SECRET_KEY=h^z13$qr_s_wd65@gnj7a=xs7t05$w7q8!x_8zsld
    DATABASE_NAME=YOUR_DATABASE__NAME
    DATABASE_USER=YOURDB_USER_NAME
    DATABASE_PASS=YOUR_DB_PASWORD`
- create migrations with: `python3 manage.py makemigrations`
- run migrations with `python3 manage.py migrate`
- run the app:  `python3 manage.py runserver`
- check http://localhost:80000


 

