from django.shortcuts import render,get_object_or_404
from django.core.paginator import Paginator
from .models import Category

def category_list(request):
    categories = Category.objects.all()
    paginator = Paginator(categories, 25)  # Show 25 contacts per page.
    page_number = request.GET.get("page")
    categories = paginator.get_page(page_number)
    return render(request, 'categories/category_list.html', {'categories': categories})
