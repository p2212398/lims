from django.shortcuts import render
from .models import Nutrient

def nutrient_list(request):
    nutrients = Nutrient.objects.all()
    return render(request, 'nutrients/nutrient_list.html', {'nutrients': nutrients})
