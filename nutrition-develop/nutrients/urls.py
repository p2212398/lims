from django.urls import path
from . import views

urlpatterns = [
    path('', views.nutrient_list, name='nutrient_list'),
]
