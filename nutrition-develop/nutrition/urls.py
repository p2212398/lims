from django.contrib import admin
from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', admin.site.urls),
   # path('products/', include('products.urls')),
    path('ingredients/', include('ingredients.urls')),
    path('recipes/', include('recipes.urls')),
    path('profile/', include('user_profiles.urls')),
    path('categories/', include('categories.urls')),
    path('foods/', include('foods.urls')),
    path('nutrients/', include('nutrients.urls')),
    path('predict/', include('predict.urls')),


]
