from django.db import models

class Ingredient(models.Model):
    name = models.CharField(max_length=255)  # Nom de l'ingrédient
    quantity = models.FloatField()           # Quantité
    unit = models.CharField(max_length=50)   # Unité (g, ml, unité)

    def __str__(self):
        return f"{self.name} ({self.quantity} {self.unit})"
