from django.shortcuts import render
from .models import Ingredient
from django import forms
import json

def ingredient_list(request):
    ingredients = Ingredient.objects.all()
    return render(request, 'ingredients/ingredient_list.html', {'ingredients': ingredients})
def ingredient_detail(request, id):
    ingredient = get_object_or_404(Ingredient, id=id)
    return render(request, 'ingredients/ingredient_detail.html', {'ingredient': ingredient})


class IngredientForm(forms.Form):
    name = forms.CharField(label="Nom de l'ingrédient", max_length=255)
    quantity = forms.FloatField(label="Quantité")
    unit = forms.ChoiceField(
        label="Unité",
        choices=[("g", "g"), ("ml", "ml"), ("unit", "unité")],
    )

class RecipeForm(forms.Form):
    ingredients = forms.CharField(
        widget=forms.Textarea,
        label="Liste des ingrédients (JSON)",
        help_text="Entrez la liste au format JSON (exemple : [{\"name\": \"sucre\", \"quantity\": 50, \"unit\": \"g\"}])",
    )

def process_ingredients(ingredients_json):
    # Convertir la liste JSON en Python
    try:
        ingredients = json.loads(ingredients_json)
    except json.JSONDecodeError:
        return {"error": "Format JSON invalide"}
    
    total_nutrients = {
        "energy": 0,
        "proteins": 0,
        "sugars": 0,
        "fiber": 0,
        "saturated_fat": 0,
        "sodium": 0,
    }

    for ingredient in ingredients:
        # Exemple : récupérer les nutriments d'Open Food Facts (mocké ici)
        data = get_nutritional_data(ingredient["name"])
        multiplier = ingredient["quantity"] / 100  # Conversion pour 100 g

        # Additionner les nutriments
        total_nutrients["energy"] += data["energy_100g"] * multiplier
        total_nutrients["proteins"] += data["proteins_100g"] * multiplier
        total_nutrients["sugars"] += data["sugars_100g"] * multiplier
        total_nutrients["fiber"] += data["fiber_100g"] * multiplier
        total_nutrients["saturated_fat"] += data["saturated-fat_100g"] * multiplier
        total_nutrients["sodium"] += data["sodium_100g"] * multiplier

    return total_nutrients

def get_nutritional_data(ingredient_name):
    # Mock de récupération des données (remplacer par une API réelle)
    database = {
        "sucre": {"energy_100g": 400, "proteins_100g": 0, "sugars_100g": 99, "fiber_100g": 0, "saturated-fat_100g": 0, "sodium_100g": 0},
        "farine": {"energy_100g": 364, "proteins_100g": 10, "sugars_100g": 0, "fiber_100g": 2.7, "saturated-fat_100g": 0, "sodium_100g": 2},
        "beurre": {"energy_100g": 717, "proteins_100g": 0.85, "sugars_100g": 0, "fiber_100g": 0, "saturated-fat_100g": 51, "sodium_100g": 11},
        "œufs": {"energy_100g": 143, "proteins_100g": 12.6, "sugars_100g": 0.37, "fiber_100g": 0, "saturated-fat_100g": 3.1, "sodium_100g": 142},
    }
    return database.get(ingredient_name.lower(), {})

from django.shortcuts import render

def predict_nutriscore_view(request):
    if request.method == "POST":
        form = RecipeForm(request.POST)
        if form.is_valid():
            ingredients_json = form.cleaned_data["ingredients"]
            nutrients = process_ingredients(ingredients_json)
            nutriscore = predict_nutriscore(nutrients)
            return render(request, "nutriscore_result.html", {"nutriscore": nutriscore})
    else:
        form = RecipeForm()
    return render(request, "predict_nutriscore.html", {"form": form})