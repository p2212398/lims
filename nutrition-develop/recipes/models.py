from django.db import models
from ingredients.models import Ingredient

class Recipe(models.Model):
    name = models.CharField(max_length=200)
    ingredients = models.ManyToManyField(Ingredient)
    instructions = models.TextField()
    estimated_nutrition = models.JSONField()  # Valeurs nutritives calculées
    predicted_nutriscore = models.CharField(max_length=1, blank=True, null=True)

    def __str__(self):
        return self.name
