from django.shortcuts import render
from .models import UserProfile

def profile(request):
    profile = UserProfile.objects.get(user=request.user)
    return render(request, 'user_profiles/profile.html', {'profile': profile})
