from django.contrib.auth.models import User
from django.db import models

class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    dietary_preferences = models.TextField()  # Ex. végétarien, sans gluten, etc.
    favorite_recipes = models.ManyToManyField('recipes.Recipe', blank=True)

    def __str__(self):
        return self.user.username
