## create venv
 -  `python3 -m venv env`
 
    ## activate venv

- `source env/bin/activate`

## installerdjango

- `pip install django`

##  creer le  fichier des dependances

- `pip freeze > requirements.txt`

## start project 
- `django-admin startproject merchex`

## run project 
- `python3 manage.py runserver`
## run migrations
- `python3 manage.py migrate`
## create django application
- `python3 manage.py startapp listings`
## add django  app to main app settings

## heritage
`class Band(models.Model):
    name=models.fields.CharField(max_lenght=100)`
## create migrations
`python3 manage.py makemigrations`
## run django shell
`python3 manage.py shell`

# install mysql
- `brew install libiconv`
- `export LIBRARY_PATH=$LIBRARY_PATH:$(brew --prefix)/lib:$(brew --prefix)/opt/libiconv/lib`
- `pip install mysqlclient`

find . -path "*/__pycache__/*.pyc"  -delete