# nutrition analysis
##  Description
Ce projet a pour objectif de mettre en place une plateforme  web permettant de donner les conseils et recommandations
sur les habitudes alimentaires saines et équilibrées basés sur le principe du nutriscore. Ces recommandation sont basées sur les prédictions d'un modèle IA que nous avons  développé et entrainé dans le cadre de ce projet
# Objectifs du projet
Amélioration des habitudes almentaires grâce aux recommandations de l'IA

# Spécifications techniques
## Technologies utilisées
- **Langage de Programmation** : Python (Python3)
- **Bibliothèques de Data Science** : `pandas`, `numpy`, `scikit-learn`, `matplotlib`, `seaborn`, `plotly`
- **Frameworks Web** : `Flask`, `Django`
- **Base de Données** : SQL MySQLpour stocker les données.
- **Outils de Déploiement** : Docker,
## Architecture:
Le projet est composée d'une partie web et d'une partie API.
la pertie API expose l'API de prédiction du nutriscore par le modele
# Comment exécuter le projet?
Environnement requis:
- Os: Windows, Linux ou Mac
- installer `docker`, et `git`
- clonner ce projet
- à la racine de ce projet  lancer la commande:
 ` docker-compose up --build`
- aller sur le lien http:localhost:8000



 

