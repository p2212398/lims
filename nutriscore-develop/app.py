from flask import Flask, request, jsonify
import joblib
import numpy as np

# Charger le modèle
model = joblib.load('nutriscore_model.pkl')

# Initialiser l'application Flask
app = Flask(__name__)

@app.route('/predict', methods=['POST'])
def predict():
    # Récupérer les données JSON envoyées
    data = request.json
    print("Predition...")

    print(data)

    try:
        # Convertir les données en numpy array
        features = np.array(data['features']).reshape(1, -1)

        # Faire une prédiction
        prediction = model.predict(features)

        return jsonify({'prediction': prediction.tolist()})
    except Exception as e:
        return jsonify({'error': str(e)}), 400

@app.route('/doc', methods=['GET'])
def doc():
    # Récupérer les données JSON envoyées
    print("Predition...")
    return jsonify({'prediction': {"status":"Model ready foor prediction","apis":[{predict:{"method":"post","url":"/predict","request_body":"Features for prediction","response":"Prediction  result"}}]}})

if __name__ == '__main__':
    app.run(debug=True)
    print("App ready for predictions...")
